package org.example.libraryappbasic.security;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Konfiguracja hasła w aplikacja
 */
@Configuration
public class PasswordConfig {
    /**
     * Tworzy obiekt PasswordEncoder dla hasła
     * @return obiekt passwordEncoder
     */
    @Bean
    public PasswordEncoder passwordEncoder(){       //ma na celu zahashowanie hasła i sprawdzenie hasła z hasłem podanym przez usera
        return new BCryptPasswordEncoder();
    }
}
