package org.example.libraryappbasic.security;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;
import java.io.IOException;
import java.util.List;

/**
 * Filtr do obsługi tokenów JWT
 */
public class JWTTokenFilter extends OncePerRequestFilter {
    private final String key;

    /**
     * Konstruktor filtru JWTTokenFilter
     * @param key klucz do weryfikacji tokenów JWT
     */
    public JWTTokenFilter(String key){
        this.key = key;
    }

    /**
     * Metoda do przetwarzania zewnętrznych żądań.
     * @param request żadanie HTTP
     * @param response odpowiedź HTTP
     * @param filterChain łańcuch filtrów
     * @throws ServletException występuje, gdy żądanie nie może być obsłużone
     * @throws IOException pojawia się, gdy występuje błąd wejścia/wyjścia podczas obsługi żądania
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String headerAuth = request.getHeader(HttpHeaders.AUTHORIZATION);
        System.out.println("Authorization header: " + headerAuth);
        if(headerAuth!=null && headerAuth.startsWith("Bearer ")){
            String token = headerAuth.split(" ")[1];
            Claims claims = Jwts.parser()
                    .setSigningKey(key)
                    .build()
                    .parseSignedClaims(token)
                    .getPayload();
            String userId = ((Integer) claims.get("id")).toString();
            String role = (String) claims.get("role");

            Authentication authentication = new UsernamePasswordAuthenticationToken(
                    userId, null, List.of(new SimpleGrantedAuthority(role))
            );
            SecurityContextHolder.getContext().setAuthentication(authentication);

        }else{
            SecurityContextHolder.getContext().setAuthentication(null);
        }
        filterChain.doFilter(request,response);
    }
}
