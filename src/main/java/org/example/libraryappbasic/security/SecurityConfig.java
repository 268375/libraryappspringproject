package org.example.libraryappbasic.security;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * Konfiguracja bezpieczeństwa dostępu do aplikacji
 */
@Configuration
@EnableWebSecurity
@EnableAutoConfiguration(exclude = {ErrorMvcAutoConfiguration.class})
public class SecurityConfig {
    @Value("${jwt.token.key}")
    private String key;

    /**
     * Konfiguracja łańcucha filtrów bezpieczeństwa
     * @param http obiekt HttpSecurity
     * @return łańcuch filtrów SecurityFilterChain
     * @throws Exception wyjątek, gdy występuje problem z konfiguracją
     */
    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        return http
                .csrf(AbstractHttpConfigurer::disable)
                .addFilterBefore(new JWTTokenFilter(key), UsernamePasswordAuthenticationFilter.class)
                .authorizeHttpRequests(
                        authorizationManagerRequestMatcherRegistry ->
                                authorizationManagerRequestMatcherRegistry
                                        .requestMatchers("/swagger-ui/**", "/v3/api-docs/**", "/swagger-ui.html").permitAll() //dostęp dla Swagger UI
                                        .requestMatchers("/login").permitAll()          //zalogować się może każdy użytkownik
                                        .requestMatchers("/user/add", "/user/delete/**", "/user/getAll").hasRole("LIBRARIAN")  //dodać, usunąć i wyświetlić wszystkich użytkowników może tylko bibliotekarz
                                        .requestMatchers("/book/add", "/book/delete/**").hasRole("LIBRARIAN")  //dodać i usunąć książke może tylko bibliotekarz
                                        .requestMatchers("/book/getAll").hasAnyRole("LIBRARIAN", "READER")   //wyświetlić wszystkie ksiażki może czytelnik i bibliotekarz
                                        .requestMatchers("/loan/add", "/loan/getAll").hasRole("LIBRARIAN")  //dodać wypożyczenie i wyświetlić wszystkie wypożyczenia może tylko bibliotekarz
                                        .requestMatchers("/loan/return/**").hasRole("LIBRARIAN")  //dodać datę zwrotu wypożyczenia może tylko bibliotekarz
                                        .requestMatchers("/loan/myLoans").hasAnyRole("LIBRARIAN", "READER")// zobaczyć swoje wypożyczenia może czytelnik i bibliotekarz
                                        .requestMatchers("/user/update").hasAnyRole("LIBRARIAN", "READER") //aktualizować swoje dane może i czytelnik i bibliotekarz
                                        .requestMatchers("/user/updateOther/**").hasRole("LIBRARIAN") //aktualizować dane innych użytkowników może bibliotekarz np. na ich prośbę
                                        .requestMatchers("/book/searchByIsbn/**", "/book/searchByAuthor/**", "book/searchByPublisher/**", "/book/searchByTitle/**", "/book/searchByPublishYear/**").hasAnyRole("LIBRARIAN", "READER")//wyszukiwać książki na podstawie różnych parametrów może czytelnik i bibliotekarz
                                        .requestMatchers("/user/InfoAboutMe").hasAnyRole("LIBRARIAN", "READER")//uzyskać informacje o sobie może czytelnik i bibliotekarz
                                        .requestMatchers("/user/findByUserName/**").hasRole("LIBRARIAN")//wyszukać uzytkownika po nazwie użytkownika może bibliotekarz
                                        .requestMatchers("/loan/userLoans/**").hasRole("LIBRARIAN")//wyświetlenie wypożyczeń dla danego czytelnika
                                        .requestMatchers("/loan/bookLoans/**").hasRole("LIBRARIAN")//wyświetlenie wypożyczeń danej książki
                                        .requestMatchers("/book/updateBookDetails/**").hasRole("LIBRARIAN")//zaktualizowanie danych książki



                )
                .exceptionHandling()
                .accessDeniedHandler((request, response, accessDeniedException) -> {
                    response.setStatus(HttpStatus.FORBIDDEN.value());
                    response.getWriter().write("As your role you are not authorized to access this resource.");
                })
                .and()

                .sessionManagement(sessionManagement ->
                        sessionManagement.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
                .build();
    }
}
