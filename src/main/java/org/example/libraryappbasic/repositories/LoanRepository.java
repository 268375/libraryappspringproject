package org.example.libraryappbasic.repositories;

import org.example.libraryappbasic.entities.Loan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Interfejs repozytorium dla encji wypożyczenia
 */

@Repository
public interface LoanRepository extends JpaRepository<Loan, Integer> {
    /**
     * Znajduje wypożyczenia na podstawie ID użytkownika
     * @param userId ID użytkownika
     * @return znalezione wypożyczenia dla danego użytkownika
     */
    Iterable<Loan> findByUserId(Integer userId);
    /**
     * Znajduje wypożyczenia na podstawie ID książki
     * @param bookId ID książki
     * @return znalezione wypożyczenia danej książki
     */
    Iterable<Loan> findByBookId(Integer bookId);
}
