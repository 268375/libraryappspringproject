package org.example.libraryappbasic.repositories;
import org.example.libraryappbasic.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Interfejs repozytorium dla encji użytkownika
 */
@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    /**
     * Znajduje użytkownika na podstawie nazwy użytkownika
     * @param loginForm nazwa użytkownika
     * @return znaleziony użytkownik
     */
    @Query(value = "SELECT u FROM User u WHERE u.userName = :loginForm")
    User findByUserName(String loginForm);
}

