package org.example.libraryappbasic.repositories;
import org.example.libraryappbasic.entities.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Interfejs repozytorium dla encji książki
 */
@Repository
public interface BookRepository extends JpaRepository<Book, Integer> {
    /**
     * Znajduje książkę po numerze ISBN
     * @param isbn numer ISBN książki
     * @return znaleziona książka o danym numerze ISBN
     */
    Book findByIsbn(String isbn);

    /**
     * Znajduje książki zawierające w tytule zadany ciąg znaków (ignoruje przy tym wielkość liter)
     * @param title tytuł szukanej książki
     * @return znaleziona książka o szukanym tytule
     */
    List<Book> findByTitleContainingIgnoreCase(String title);  //nie uwzględniam wielkości liter

    /**
     * Znajduje książki wydane w danym roku
     * @param year rok wydania poszukiwanych książek
     * @return lista książek wydanych w danym roku
     */
    List<Book> findByPublishYear(Integer year);

    /**
     * Znajduje książki wydane przez wydawnictwo zawierające podany ciąg znaków w nazwie wydawnictwa
     * @param publisher szukana nazwa wydawnictwa
     * @return lista znalezionych książek danego wydawnictwa
     */
    List<Book> findByPublisherContainingIgnoreCase(String publisher);

    /**
     * Znajduje książki, któryc autor zawiera zadany ciąg znaków w nazwie
     * @param author część lub cała nazwa autora książki
     * @return lista znalezionych książek napisanych przez danego autora
     */
    List<Book> findByAuthorContainingIgnoreCase(String author);

}

