package org.example.libraryappbasic;

/**
 * Klasa reprezentująca formularz logowania (zawiera pola dla loginu i hasła)
 */
public class LoginForm {
    private String login;
    private String password;

    /**
     * Metoda pobierająca nazwę użytkownika
     * @return nazwa użytkownika
     */

    public String getLogin() {
        return login;
    }

    /**
     * Metoda ustawiająca nazwę użytkownika
     * @param login nazwa użytkownika
     */

    public void setLogin(String login) {
        this.login = login;
    }
    /**
     * Metoda pobierająca hasło użytkownika
     * @return hasło użytkownika
     */

    public String getPassword() {
        return password;
    }

    /**
     * Metoda ustawiająca hasło użytkownika
     * @param password hasło użytkownika
     */

    public void setPassword(String password) {
        this.password = password;
    }
}
