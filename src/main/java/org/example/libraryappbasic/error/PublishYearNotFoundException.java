package org.example.libraryappbasic.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;
/**
 * Wyjątek rzucany, gdy rok wydania książki szukany przez użytkowika biblioteki nie zostanie znaleziony
 */

public class PublishYearNotFoundException extends RuntimeException{
    private PublishYearNotFoundException(Integer year){
        super("Books published in year " + year + " not found");
    }
    /**
     * Utworzenie nowego wyjątku PublishYearNotFoundException
     * @param year rok wydania książki, który nie został znaleziony
     * @return wyjątek ResponseStatusException z kodem HTTP i wiadomością dla użytkownika
     */
    public static ResponseStatusException create(Integer year){
        PublishYearNotFoundException exception = new PublishYearNotFoundException(year);
        return new ResponseStatusException(HttpStatus.NOT_FOUND,exception.getMessage(), exception);

    }
}
