package org.example.libraryappbasic.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;
/**
 * Wyjątek rzucany, gdy informacje o wypożyczeniu są niekompletne
 */

public class IncompleteLoanDetailsException extends RuntimeException{
    private IncompleteLoanDetailsException(String message){super(message);}
    /**
     * Utworzenie nowego wyjątku IncompleteLoanDetailsException
     * @return wyjątek ResponseStatusException z kodem HTTP i wiadomością dla użytkownika
     */
    public static ResponseStatusException create(){
        IncompleteLoanDetailsException exception = new IncompleteLoanDetailsException(String.format("Incomplete information about loan"));
        return new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage(), exception);
    }


}
