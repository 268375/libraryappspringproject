package org.example.libraryappbasic.error;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;
/**
 * Wyjątek rzucany, gdy informacje o użytkowniku są niekompletne
 */
public class IncompleteUserDetailsException extends RuntimeException{
    private IncompleteUserDetailsException(String message){super(message);}
    /**
     * Utworzenie nowego wyjątku IncompleteUserDetailsException
     * @return wyjątek ResponseStatusException z kodem HTTP i wiadomością dla użytkownika
     */
    public static ResponseStatusException create(){
        IncompleteUserDetailsException exception = new IncompleteUserDetailsException(String.format("Incomplete information about user"));
        return new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage(), exception);
}


}
