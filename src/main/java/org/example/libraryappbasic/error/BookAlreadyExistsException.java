package org.example.libraryappbasic.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * Wyjątek rzucany, gdy książa którą chcemy dodać już istnieje w bibliotece
 */

public class BookAlreadyExistsException extends RuntimeException{
    private BookAlreadyExistsException(String message) {
        super(message);
    }

    /**
     * Utworzenie wyjątku BookAlreadyExistsException
     * @param isbn numer ISBN książki, która już figuruje w bibliotece
     * @return wyjątek ResponseStatusException z kodem HTTP i wiadomością dla użytkownika
     */
    public static ResponseStatusException create(String isbn){
        BookAlreadyExistsException exception = new BookAlreadyExistsException(String.format("Book with isbn %s already exists.", isbn));
        return new ResponseStatusException(HttpStatus.CONFLICT, exception.getMessage(), exception);
    }
}
