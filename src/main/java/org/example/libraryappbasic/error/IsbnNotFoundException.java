package org.example.libraryappbasic.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;
/**
 * Wyjątek rzucany, gdy numer ISBN szukany przez użytkowika biblioteki nie zostanie znaleziony
 */
public class IsbnNotFoundException extends RuntimeException{
    private IsbnNotFoundException (String isbn){
        super("ISBN '" + isbn + "' not found");}
    /**
     * Utworzenie nowego wyjątku IsbnNotFoundException
     * @param isbn numer ISBN, który nie został znaleziony
     * @return wyjątek ResponseStatusException z kodem HTTP i wiadomością dla użytkownika
     */
    public static ResponseStatusException create(String isbn){
        IsbnNotFoundException exception = new IsbnNotFoundException(isbn);
        return new ResponseStatusException(HttpStatus.NOT_FOUND, exception.getMessage(), exception);
    }


}
