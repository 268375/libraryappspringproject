package org.example.libraryappbasic.error;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * Wyjątek rzucany, gdy wprowadzone przez użytkownika dane logowania są nieprawidłowe
 */
public class WrongLoginDetailsException extends RuntimeException{
    private WrongLoginDetailsException(String message){super(message);
}

    /**
     * Utworzenie nowego wyjątku WrongLoginDetailsException
     * @return wyjątek ResponseStatusException z kodem HTTP i wiadomością dla użytkownika
     */
    public static ResponseStatusException create(){
        WrongLoginDetailsException exception = new WrongLoginDetailsException("Wrong login and/or password");
        return new ResponseStatusException(HttpStatus.UNAUTHORIZED, exception.getMessage(), exception);
}

}
