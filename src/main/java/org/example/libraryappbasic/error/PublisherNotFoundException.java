package org.example.libraryappbasic.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;
/**
 * Wyjątek rzucany, gdy wydawnictwo szukane przez użytkowika biblioteki nie zostanie znalezione
 */
public class PublisherNotFoundException extends RuntimeException{
    private PublisherNotFoundException(String publisher){
        super("Publisher '" + publisher + "' not found");}
    /**
     * Utworzenie nowego wyjątku PublisherNotFoundException
     * @param publisher nazwa wydawnictwa, które nie zostało znalezione
     * @return wyjątek ResponseStatusException z kodem HTTP i wiadomością dla użytkownika
     */
    public static ResponseStatusException create(String publisher){
        PublisherNotFoundException exception = new PublisherNotFoundException(publisher);
        return new ResponseStatusException(HttpStatus.NOT_FOUND, exception.getMessage(), exception);
    }


}
