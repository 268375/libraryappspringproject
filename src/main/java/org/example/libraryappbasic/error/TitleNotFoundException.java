package org.example.libraryappbasic.error;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;
/**
 * Wyjątek rzucany, gdy tytuł książki szukany przez użytkowika biblioteki nie zostanie znaleziony
 */
public class TitleNotFoundException extends RuntimeException{
    private TitleNotFoundException(String title){
        super("Title '" + title + "' not found");}
    /**
     * Utworzenie nowego wyjątku TitleNotFoundException
     * @param title tytuł książki, który nie został znaleziony
     * @return wyjątek ResponseStatusException z kodem HTTP i wiadomością dla użytkownika
     */
    public static ResponseStatusException create(String title){
        TitleNotFoundException exception = new TitleNotFoundException(title);
        return new ResponseStatusException(HttpStatus.NOT_FOUND, exception.getMessage(), exception);
    }


}
