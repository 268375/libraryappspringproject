package org.example.libraryappbasic.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;
/**
 * Wyjątek rzucany, gdy użytkownik którego chcemy dodać już istnieje w bibliotece
 */

public class UserAlreadyExistsException extends RuntimeException{
    private UserAlreadyExistsException(String message) {
        super(message);
    }
    /**
     * Utworzenie wyjątku UserAlreadyExistsException
     * @param userName nazwa użytkownika, który już figuruje w bibliotece
     * @return wyjątek ResponseStatusException z kodem HTTP i wiadomością dla użytkownika
     */
    public static ResponseStatusException create(String userName){
        UserAlreadyExistsException exception = new UserAlreadyExistsException(String.format("User with username %s already exists.", userName));
        return new ResponseStatusException(HttpStatus.CONFLICT, exception.getMessage(), exception);
    }
}
