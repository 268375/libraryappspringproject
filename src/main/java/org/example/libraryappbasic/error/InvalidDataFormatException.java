package org.example.libraryappbasic.error;

/**
 * Wyjątek rzucany, gdy format danych jest nieprawidłowy
 */

public class InvalidDataFormatException extends RuntimeException {
    private InvalidDataFormatException(String message) {
        super(message);
    }

    /**
     * Utworzenie nowego wyjątku InvalidDataFormatException
     * @param field pole, w którym wystąpił nieprawidłowy format danych
     * @return wyjątek InvalidDataFormatException z wiadomością dla użytkownika
     */
    public static InvalidDataFormatException create(String field) {
        return new InvalidDataFormatException(String.format("Invalid format in field '%s'.", field));
    }
}


