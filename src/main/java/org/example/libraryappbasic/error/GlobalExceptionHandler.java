package org.example.libraryappbasic.error;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.server.ResponseStatusException;

/**
 * Obsługuje wyjątki z aplikacji
 */
@RestControllerAdvice
public class GlobalExceptionHandler {
    /**
     * Obsługuje wyjątki ResponseStstusException
     * @param ex wyjątek ResponseStatusException do obsłużenia
     * @return ResoponseEntity z odpowiednim statusem i powodem
     */
    @ExceptionHandler(ResponseStatusException.class)
    public ResponseEntity<String> handleResponseStatusException(ResponseStatusException ex) {
        return ResponseEntity.status(ex.getStatusCode()).body(ex.getReason());
    }

    /**
     * Obsługuje wyjątki InvalidDataFormatException
     * @param ex wyjątek InvalidDataFormatException do obsużenia
     * @return ResponseEntity ze statusem BAD REQUEST i wiadomością
     */
    @ExceptionHandler(InvalidDataFormatException.class)
    public ResponseEntity<String> handleInvalidDataFormatException(InvalidDataFormatException ex) {
        return ResponseEntity
                .badRequest()
                .body(ex.getMessage());
    }
}



