package org.example.libraryappbasic.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;
/**
 * Wyjątek rzucany, gdy użytkownik nie istnieje
 */
public class UserDoesntExistException extends RuntimeException{
    private UserDoesntExistException(String message) {
        super(message);
    }
    /**
     * Utworzenie nowego wyjątku UserDoesntExistException
     * @param userId id użytkownika, który nie istnieje w bibliotece
     * @return wyjątek ResponseStatusException z kodem HTTP i wiadomością dla użytkownika
     */
    public static ResponseStatusException create(Integer userId){
        UserDoesntExistException exception = new UserDoesntExistException(String.format("User with ID %d doesn't exist.", userId));
        return new ResponseStatusException(HttpStatus.NOT_FOUND, exception.getMessage(), exception);
    }
}
