package org.example.libraryappbasic.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * Wyjątek rzucany, gdy autor szukany przez użytkowika biblioteki nie zostanie znaleziony
 */
public class AuthorNotFoundException extends RuntimeException{
    private AuthorNotFoundException(String author){
        super("Author '" + author + "' not found");}

    /**
     * Utworzenie nowego wyjątku AuthorNotFoundException
     * @param author nazwa autora, który nie został znaleziony
     * @return wyjątek ResponseStatusException z kodem HTTP i wiadomością dla użytkownika
     */
    public static ResponseStatusException create(String author){
        AuthorNotFoundException exception = new AuthorNotFoundException(author);
        return new ResponseStatusException(HttpStatus.NOT_FOUND, exception.getMessage(), exception);
    }


}
