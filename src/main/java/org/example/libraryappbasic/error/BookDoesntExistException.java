package org.example.libraryappbasic.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * Wyjątek rzucany, gdy książka nie istnieje
 */

public class BookDoesntExistException extends RuntimeException{

    private BookDoesntExistException(String message) {
        super(message);
    }

    /**
     * Utworzenie nowego wyjątku BookDoesntExistException
     * @param bookId id książki, która nie istnieje w bibliotece
     * @return wyjątek ResponseStatusException z kodem HTTP i wiadomością dla użytkownika
     */
    public static ResponseStatusException create(Integer bookId){
        BookDoesntExistException exception = new BookDoesntExistException(String.format("Book with ID %d doesn't exist.", bookId));
        return new ResponseStatusException(HttpStatus.NOT_FOUND, exception.getMessage(), exception);
    }
}
