package org.example.libraryappbasic.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * Wyjątek rzucany, gdy informacje o książce są niekompletne
 */
public class IncompleteBookDetailsException extends RuntimeException {
    private IncompleteBookDetailsException(String message){super(message);}

    /**
     * Utworzenie nowego wyjątku IncompleteBookDetailsException
     * @return wyjątek ResponseStatusException z kodem HTTP i wiadomością dla użytkownika
     */
    public static ResponseStatusException create(){
        IncompleteBookDetailsException exception = new IncompleteBookDetailsException(String.format("Incomplete information about book"));
        return new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage(), exception);
    }
}
