package org.example.libraryappbasic.service;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.example.libraryappbasic.LoginForm;
import org.example.libraryappbasic.entities.User;
import org.example.libraryappbasic.error.WrongLoginDetailsException;
import org.example.libraryappbasic.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.Date;

/**
 * Serwis obsługujący proces logowania się użytkowników do biblioteki
 */
@Service
public class LoginService {
    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;
    @Value("${jwt.token.key}")
    private String key;

    /**
     * Konstruktor serwisu LoginService
     * @param passwordEncoder encoder hasła
     * @param userRepository repozytorium użytkowników
     */
    @Autowired
    public LoginService(PasswordEncoder passwordEncoder, UserRepository userRepository){
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;

    }

    /**
     * Metoda służąca do logowania się użytkowników
     * @param loginForm dane do logowania użytkowników
     * @return token JWT po zalogowaniu
     * @throws WrongLoginDetailsException wyjątek pojawiający się w przypadku podania błędnych danych do logowania
     */
    public String userLogin(LoginForm loginForm){
        User user = userRepository.findByUserName(loginForm.getLogin());

        if(user != null && passwordEncoder.matches(loginForm.getPassword()
                ,user.getPassword())){ //tutaj wyciągamy hasło z bazy danych
            long timeMilis = System.currentTimeMillis();
            String token = Jwts.builder()
                    .issuedAt(new Date(timeMilis))
                    .expiration(new Date(timeMilis+30*60*1000)) //czas ważności tokena ustawiony na 30 minut
                    .claim("id", user.getId())  //w tokenie zapisany identyfikator użytkownika
                    .claim("role", user.getRole())  //w tokenie zapisana rola użytkownika
                    .signWith(SignatureAlgorithm.HS256, key)
                    .compact();
            return token;

        }else{
            throw WrongLoginDetailsException.create();
        }

    }
}
