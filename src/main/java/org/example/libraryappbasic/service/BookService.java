package org.example.libraryappbasic.service;
import org.example.libraryappbasic.entities.Book;
import org.example.libraryappbasic.entities.User;
import org.example.libraryappbasic.error.*;
import org.example.libraryappbasic.repositories.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.Year;

/**
 * Serwis obsługujący operacje na książkach
 */

@Service
public class BookService {
    private BookRepository bookRepository;
    private PasswordEncoder passwordEncoder;
    @Value("${jwt.token.key}")
    private String key;

    /**
     * Konstruktor serwisu BookService
     * @param bookRepository repozytorium książek
     * @param passwordEncoder encoder hasła
     */
    @Autowired
    public BookService(BookRepository bookRepository, PasswordEncoder passwordEncoder) {
        this.bookRepository = bookRepository;
        this.passwordEncoder = passwordEncoder;
    }

    /**
     * Metoda do zapisu nowej książki nowej książki
     * @param book nowa książka do zapisania
     * @return dodana książka
     * @throws IncompleteBookDetailsException wyjątek pojawiający się, gdy zostaną podane niekompletne dane dotyczące książki
     * @throws InvalidDataFormatException wyjątek, gdy książka do zapisania do bazy danych ma dane w ieprawidłowym formacie
     * @throws BookAlreadyExistsException wyjątek, gdy książka o danym ISBN już istnieje w bazie danych
     */
    public Book save(Book book) {                   //dodanie nowej książki
        if (book.getIsbn() == null || book.getTitle() == null || book.getAuthor() == null || book.getPublisher() == null || book.getPublishYear() == null || book.getAvailableCopies() == null) {
            throw IncompleteBookDetailsException.create();
        }
        if (!isIsbnValid(book.getIsbn())) {
            throw InvalidDataFormatException.create("ISBN");
        }
        if (!isTitleValid(book.getTitle())) {
            throw InvalidDataFormatException.create("title");
        }
        if (!isAuthorValid(book.getAuthor())) {
            throw InvalidDataFormatException.create("author");
        }

        if (!isPublisherValid(book.getPublisher())) {
            throw InvalidDataFormatException.create("publisher");
        }
        if (!isPublishYearValid(book.getPublishYear())) {
            throw InvalidDataFormatException.create("publishYear");
        }
        if (!isAvailableCopiesValid(book.getAvailableCopies())) {
            throw InvalidDataFormatException.create("availableCopies");
        }

        Book existingBook = bookRepository.findByIsbn(book.getIsbn());
        if (existingBook != null) {
            throw BookAlreadyExistsException.create(book.getIsbn());

        }
        return bookRepository.save(book);
    }

    /**
     * Sprawdzenie poprawności formatu numeru ISBN
     * @param isbn numer ISBN
     * @return True, jeśli format jest poprawny, w przeciwnym razie false
     */
    private boolean isIsbnValid(String isbn) {
        //czy ISBN ma dokładnie 13 cyfr
        return isbn != null && isbn.matches("^\\d{13}$");
    }

    /**
     * Sprawdza poprawność tytułu książki
     * @param title tytuł książki
     * @return True, jeśli tytuł książki jest zgodny z wymaganiami, w przeciwnym razie false
     */
    private boolean isTitleValid(String title) {
        //minimalna długość 1 znak, maksymalna długość 100 znaków
        return title.length() >= 1 && title.length() <= 100;
    }

    /**
     * Sprawdzenie poprawności wpisanego autora książki
     * @param author autor książki
     * @return True, jeśli format zapisu autora jest prawidłowy, w przeciwnym razie false
     */
    private boolean isAuthorValid(String author) {
        // minimalna długość 3 znaki, maksymalna długość 50 znaków
        return author != null && author.length() >= 3 && author.length() <= 50;
    }

    /**
     * Sprawdzenie poprawności wpisanego wydawnictwa książki
     * @param publisher wydawnictwo książki
     * @return True, jeśli wydawnicto jest w poprawnym formacie, w przeciwnym razie false
     */
    private boolean isPublisherValid(String publisher) {
        //minimalna długość 3 znaki, maksymalna długość 50 znaków
        return publisher != null && publisher.length() >= 3 && publisher.length() <= 50;
    }

    /**
     * Sprawdzenie poprawności roku wydania książki
     * @param publishYear rok wydania książki
     * @return True, jeśli rok wydania jest poprawny, w przeciwnym razie false
     */
    private boolean isPublishYearValid(Integer publishYear) {
        //sprawdzenie, czy rok nie jest ujemny ani większy od obecnego
        return publishYear >= 0 && publishYear <= Year.now().getValue();
    }

    /**
     * Sprawdzenie poprawności liczby dostęnych egzemplarzy książek
     * @param availableCopies dostępne egzemplarze książki
     * @return True, jeśli liczba kopii jest poprawna, w przeciwnym razie false
     */
    private boolean isAvailableCopiesValid(Integer availableCopies) {
        // sprawdzenie, czy liczba jest nieujemna, itp.
        return availableCopies > 0;
    }

    /**
     * Metpda służąca do usunięcia książki z bazy danych na podstawie id
     *
     * @param bookId id książki, która ma zostać usunięta
     * @throws BookDoesntExistException wyjątek rzucany, gdy książka o podanym id nie istnieje w bazie danych biblioteki
     */
    public void deleteById(Integer bookId) {                //usunięcie ksiązki po Id
        Book book = bookRepository.findById(bookId).orElse(null);
        if(book == null){
            throw BookDoesntExistException.create(bookId);
        }
        bookRepository.deleteById(bookId);
    }
    /**
     * Metoda zwraca wszystkie książki znajdujące się w bazie danych.
     *
     * @return Iterable zawierający wszystkie książki.
     */
    public Iterable<Book> findAll() {                       //wyświetlenie wszystkich książek
        return bookRepository.findAll();
    } //wyświetlenie wszystkich książek
    /**
     * Metoda znajduje książki na podstawie podanego tytułu
     *
     * @param title tytuł książki do wyszukania
     * @return Iterable zawierający znalezione książki
     * @throws TitleNotFoundException wyjątek rzucany, gdy żadna książka nie pasuje do podanego tytułu
     */
    //metoda do wyszukiwania książek na podstawie tytułu (jeśli tytuł, który użytkownik wyszukuje nie istnieje w bazie - komunikat o wyjątku)
    public Iterable<Book> findByTitle(String title) {
        Iterable<Book>books = bookRepository.findByTitleContainingIgnoreCase(title);
        if (!books.iterator().hasNext()){
            throw TitleNotFoundException.create(title);
        }
        return books;
    }
    /**
     * Metoda znajduje książkę na podstawie podanego ISBN
     *
     * @param isbn ISBN książki do wyszukania
     * @return znaleziona książka
     * @throws IsbnNotFoundException Wyjątek rzucany, gdy książka o podanym ISBN nie istnieje
     */
    //metoda do wyszukiwania książki na podstawie ISBN
    public Book findByIsbn(String isbn) {
        Book book = bookRepository.findByIsbn(isbn);
        if (book == null){
            throw IsbnNotFoundException.create(isbn);
        }
        return book;
    }
    /**
     * Metoda znajduje książki na podstawie podanego roku wydania
     *
     * @param year Rok wydania książki do wyszukania
     * @return Iterable zawierający znalezione książki
     * @throws PublishYearNotFoundException Wwjątek rzucany, gdy żadna książka nie pasuje do podanego roku wydania.
     */
    //metoda do wyszukiwania książek na podstawie roku wydania (jeśli rok wydania, który użytkownik wyszukuje nie istnieje w bazie - komunikat o wyjątku)
    public Iterable<Book> findByPublishYear(Integer year) {
        Iterable<Book>books = bookRepository.findByPublishYear(year);
        if (!books.iterator().hasNext()){
            throw PublishYearNotFoundException.create(year);
        }
        return books;
    }
    /**
     * Metoda znajduje książki na podstawie podanego wydawnictwa.
     *
     * @param publisher Wydawnictwo książki do wyszukania.
     * @return Iterable zawierający znalezione książki.
     * @throws PublisherNotFoundException Wyjątek rzucany, gdy żadna książka nie pasuje do podanego wydawnictwa
     */
    //metoda do wyszukiwania książek na podstawie wydawnictwa (jeśli wydawnictwo, które uzytkownik wyszukuje nie istnieje w bazie - komunikat o wyjątku)
    public Iterable<Book> findByPublisher(String publisher) {
        Iterable<Book>books = bookRepository.findByPublisherContainingIgnoreCase(publisher);
        if (!books.iterator().hasNext()){
            throw PublisherNotFoundException.create(publisher);
        }
        return books;
    }
    /**
     * Metoda znajduje książki na podstawie podanego autora
     *
     * @param author autor książki do wyszukania
     * @return Iterable zawierający znalezione książki
     * @throws AuthorNotFoundException wyjątek rzucany, gdy żadna książka nie pasuje do podanego autora
     */
    //metoda do wyszukiwania książek na podstawie autora (jeśli autor, którego uzytkownik wyszukuje nie istnieje w bazie - komunikat o wyjątku)
    public Iterable<Book> findByAuthor(String author) {
        Iterable<Book>books = bookRepository.findByAuthorContainingIgnoreCase(author);
        if (!books.iterator().hasNext()){
            throw AuthorNotFoundException.create(author);
        }
        return books;
    }
    /**
     * Metoda służąca do zaktualizowania danych książki przez bibliotekarza
     *
     * @param bookId          id książki, której dane chce zaktualizwować
     * @param updatedBookData dane książki do zaktualizowania
     * @return zaktualizowany obiekt książki
     * @throws BookDoesntExistException   jeśli książka o danym id nie istnieje w bazie danych
     * @throws BookAlreadyExistsException  jeśli książka o danym ISBN już istnieje w bazie danych
     * @throws InvalidDataFormatException jeśli format danych do aktualizacji jest niepoprawny
     */
    public Book updateBook(Integer bookId, Book updatedBookData) {
        Book existingBook = bookRepository.findById(bookId).orElse(null);
        if (existingBook == null) {
            throw BookDoesntExistException.create(bookId);
        }
        if (!existingBook .getIsbn().equals(updatedBookData.getIsbn())) {
            Book bookWithSameIsbn = bookRepository.findByIsbn(updatedBookData.getIsbn());
            if (bookWithSameIsbn != null) {
                throw BookAlreadyExistsException.create(updatedBookData.getIsbn());
            }
        }
        updateBookFields(existingBook, updatedBookData);
        return bookRepository.save(existingBook);
    }



    /**
     * Metoda aktualizująca pola książki
     *
     * @param existingBook istniejący obiekt książki
     * @param updatedBookData nowe dane książki
     * @throws InvalidDataFormatException jeśli nowe dane książki są w nieprawidłowym formacie
     */
    private void updateBookFields(Book existingBook, Book updatedBookData) {

        if (updatedBookData.getIsbn()!= null) {
            if (!isIsbnValid(updatedBookData.getIsbn())) {
                throw InvalidDataFormatException.create("isbn");
            }
            existingBook.setIsbn(updatedBookData.getIsbn());
        }
        if (updatedBookData.getTitle()!= null) {
            if (!isTitleValid(updatedBookData.getTitle())) {
                throw InvalidDataFormatException.create("title");
            }
            existingBook.setTitle(updatedBookData.getTitle());
        }
        if (updatedBookData.getAuthor()!= null) {
            if (!isAuthorValid(updatedBookData.getAuthor())) {
                throw InvalidDataFormatException.create("author");
            }
            existingBook.setAuthor(updatedBookData.getAuthor());
        }
        if (updatedBookData.getPublisher()!= null) {
            if (!isPublisherValid(updatedBookData.getPublisher())) {
                throw InvalidDataFormatException.create("publisher");
            }
            existingBook.setPublisher(updatedBookData.getPublisher());
        }
        if (updatedBookData.getPublishYear()!= null) {
            if (!isPublishYearValid(updatedBookData.getPublishYear())) {
                throw InvalidDataFormatException.create("publishYear");
            }
            existingBook.setPublishYear(updatedBookData.getPublishYear());
        }
        if (updatedBookData.getAvailableCopies()!= null) {
            if (!isAvailableCopiesValid(updatedBookData.getAvailableCopies())) {
                throw InvalidDataFormatException.create("availableCopies");
            }
            existingBook.setAvailableCopies(updatedBookData.getAvailableCopies());
        }
    }





}


