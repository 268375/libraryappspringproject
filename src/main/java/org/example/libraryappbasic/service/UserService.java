package org.example.libraryappbasic.service;

import org.example.libraryappbasic.entities.EnumRoleUser;
import org.example.libraryappbasic.entities.User;
import org.example.libraryappbasic.error.IncompleteUserDetailsException;
import org.example.libraryappbasic.error.InvalidDataFormatException;
import org.example.libraryappbasic.error.UserAlreadyExistsException;
import org.example.libraryappbasic.error.UserDoesntExistException;
import org.example.libraryappbasic.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.Optional;

/**
 * Serwis obsługujący operacje na użytkownikach biblioteki
 */

@Service
public class UserService {
    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;
    @Value("${jwt.token.key}")
    private String key;

    /**
     * Konstruktor serwisu UserService
     *
     * @param userRepository  repozytorium użytkowników
     * @param passwordEncoder encoder hasła
     */
    @Autowired
    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    /**
     * Metoda służąca do zapisu użytkownika do bazy danych biblioteki
     *
     * @param user użytkownik do zapisania
     * @return zapisany użytkownik
     * @throws IncompleteUserDetailsException wyjątek w przypadku próby zapisu użytkownika z niekompletnymi danymi
     * @throws InvalidDataFormatException     wyjątek w przypadku próby zapisu użytkownika z błędnymi wartościami w którymś z pól
     * @throws UserAlreadyExistsException     wyjątek, gdy użytkownik, którego chcemy zapisać już istnieje w bazie
     */
    public User save(User user) {   //zaszyfrowanie hasła przed zapisaniem do bazy danych
        if (user.getUserName() == null || user.getPassword() == null || user.getRole() == null || user.getEmail() == null || user.getFullUserName() == null) {
            throw IncompleteUserDetailsException.create();
        }
        if (!isUsernameValid(user.getUserName())) {
            throw InvalidDataFormatException.create("username");
        }
        if (!isPasswordValid(user.getPassword())) {
            throw InvalidDataFormatException.create("password");
        }
        if (!isFullUsernameValid(user.getFullUserName())) {
            throw InvalidDataFormatException.create("fullUserName");
        }

        if (!isEmailValid(user.getEmail())) {
            throw InvalidDataFormatException.create("email");
        }

        User existingUser = userRepository.findByUserName(user.getUserName());
        if (existingUser != null) {
            throw UserAlreadyExistsException.create(user.getUserName());
        }
        if (!isValidRole(user.getRole())) {
            throw InvalidDataFormatException.create("role");
        }
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }

    private boolean isValidRole(EnumRoleUser role) {
        return role == EnumRoleUser.ROLE_READER || role == EnumRoleUser.ROLE_LIBRARIAN;
    }


    /**
     * Metoda usuwająca użytkownika z bazy danych na podstawie jego ID
     *
     * @param userId id użytkownika do usunięcia z bazy danych
     * @throws UserDoesntExistException wyjątek, gdy id użytkownika, którego chcemy usunąć nie istnieje
     */
    public void deleteById(Integer userId) {
        User user = userRepository.findById(userId).orElse(null);
        if (user == null) {
            throw UserDoesntExistException.create(userId);
        }
        userRepository.deleteById(userId);
    }

    /**
     * Metoda zwracająca wszystkich użytkowników bazy danych biblioteki
     *
     * @return wszyscy użytkownicy biblioteki
     */
    public Iterable<User> findAll() {
        return userRepository.findAll();
    }

    /**
     * Metoda zwracająca użytkownika z bazy danych na podstawie jego id
     *
     * @param userId id użytkownika do wyszukania
     * @return Optional zawierający użytkownika, jeśli istnieje, lub pusty
     */

    public Optional<User> findById(Integer userId) {
        return userRepository.findById(userId);
    }


    /**
     * Metoda służąca do zaktualizowania własnych danych przez użytkownika
     *
     * @param userId          id użytkownika, który chce zaktualizwować swoje dane
     * @param updatedUserData dane użytkownika do zaktualizowania
     * @return zaktualizowany obiekt użytkownika
     * @throws UserDoesntExistException   jeśli użytkownik o danym id nie istnieje w bazie danych
     * @throws UserAlreadyExistsException  jeśli użytkownik o danej nazwie już istnieje w bazie danych
     * @throws InvalidDataFormatException jeśli format danych do aktualizacji jest niepoprawny
     */
    public User updateUser(Integer userId, User updatedUserData) {
        User existingUser = userRepository.findById(userId).orElse(null);
        if (existingUser == null) {
            throw UserDoesntExistException.create(userId);
        }
        if (!existingUser.getUserName().equals(updatedUserData.getUserName())) {
            User userWithSameUsername = userRepository.findByUserName(updatedUserData.getUserName());
            if (userWithSameUsername != null) {
                throw UserAlreadyExistsException.create(updatedUserData.getUserName());
            }
        }
        updateUserFields(existingUser, updatedUserData);
        return userRepository.save(existingUser);
    }


    /**
     * Metoda służąca do zaktualizowania danych użytkownika przez bibliotekarza  (bibliotekarz może zmieniać również role użytkowników)
     *
     * @param userId          id użytkownika, którego dane chce zaktualizwować
     * @param updatedUserData dane użytkownika do zaktualizowania
     * @return zaktualizowany obiekt użytkownika
     * @throws UserDoesntExistException   jeśli użytkownik o danym id nie istnieje w bazie danych
     * @throws UserAlreadyExistsException  jeśli użytkownik o danej nazwie już istnieje w bazie danych
     * @throws InvalidDataFormatException jeśli format danych do aktualizacji jest niepoprawny
     */
    public User updateOtherUser(Integer userId, User updatedUserData) {
        User existingUser = userRepository.findById(userId).orElse(null);
        if (existingUser == null) {
            throw UserDoesntExistException.create(userId);
        }
        if (!existingUser.getUserName().equals(updatedUserData.getUserName())) {
            User userWithSameUsername = userRepository.findByUserName(updatedUserData.getUserName());
            if (userWithSameUsername != null) {
                throw UserAlreadyExistsException.create(updatedUserData.getUserName());
            }
        }
        updateUserFieldsLibrarian(existingUser, updatedUserData);
        return userRepository.save(existingUser);
    }

    /**
     * Metoda służąca do znalezienia użytkownika w bazie danych na podstawie jego nazwy użytkownika
     *
     * @param userName nazwa użytkownika do znalezienia
     * @return obiekt użytkownika, jeśli istnieje użytkownik o podanej nazwie, w przeciwnym razie null
     */
    public User findByUserName(String userName) {
        return userRepository.findByUserName(userName);
    }

    /**
     * Metoda aktualizująca pola użytkownika
     *
     * @param existingUser istniejący obiekt użytkownika
     * @param updatedUserData nowe dane użytkownika
     * @throws InvalidDataFormatException jeśli nowe dane użytkownika są w nieprawidłowym formacie
     */
    private void updateUserFields(User existingUser, User updatedUserData) {
        if (updatedUserData.getPassword() != null) {
            if (!isPasswordValid(updatedUserData.getPassword())) {
                throw InvalidDataFormatException.create("password");
            }
            existingUser.setPassword(passwordEncoder.encode(updatedUserData.getPassword()));
        }
        if (updatedUserData.getUserName() != null) {
            if (!isUsernameValid(updatedUserData.getUserName())) {
                throw InvalidDataFormatException.create("username");
            }
            existingUser.setUserName(updatedUserData.getUserName());
        }
        if (updatedUserData.getFullUserName() != null) {
            if (!isFullUsernameValid(updatedUserData.getFullUserName())) {
                throw InvalidDataFormatException.create("fullUserName");
            }
            existingUser.setFullUserName(updatedUserData.getFullUserName());
        }
        if (updatedUserData.getEmail() != null) {
            if (!isEmailValid(updatedUserData.getEmail())) {
                throw InvalidDataFormatException.create("email");
            }
            existingUser.setEmail(updatedUserData.getEmail());
        }
    }
    /**
     * Metoda aktualizująca pola użytkownika przez bibliotekarza
     *
     * @param existingUser istniejący obiekt użytkownika
     * @param updatedUserData nowe dane użytkownika
     * @throws InvalidDataFormatException jeśli nowe dane użytkownika są w nieprawidłowym formacie
     */
    private void updateUserFieldsLibrarian(User existingUser, User updatedUserData) {
        if (updatedUserData.getPassword() != null) {
            if (!isPasswordValid(updatedUserData.getPassword())) {
                throw InvalidDataFormatException.create("password");
            }
            existingUser.setPassword(passwordEncoder.encode(updatedUserData.getPassword()));
        }
        if (updatedUserData.getUserName() != null) {
            if (!isUsernameValid(updatedUserData.getUserName())) {
                throw InvalidDataFormatException.create("username");
            }
            existingUser.setUserName(updatedUserData.getUserName());
        }
        if (updatedUserData.getFullUserName() != null) {
            if (!isFullUsernameValid(updatedUserData.getFullUserName())) {
                throw InvalidDataFormatException.create("fullUserName");
            }
            existingUser.setFullUserName(updatedUserData.getFullUserName());
        }
        if (updatedUserData.getEmail() != null) {
            if (!isEmailValid(updatedUserData.getEmail())) {
                throw InvalidDataFormatException.create("email");
            }
            existingUser.setEmail(updatedUserData.getEmail());
        }
        if (updatedUserData.getRole() != null) {
            if (!isValidRole(updatedUserData.getRole())) {
                throw InvalidDataFormatException.create("role");
            }
            existingUser.setRole(updatedUserData.getRole());
        }
    }
    /**
     * Metoda sprawdzająca poprawność formatu nazwy użytkownika
     * @param username nazwa użytkownika
     * @return true, jeśli nazwa użytkownika jest poprawna, jeśli nie false
     */
    //nazwa użytkownika może zawierać małe i duże litery, cyfry oraz znaki specjalne: '-' i '_' i musi mieć od 3 do 16 znaków
    private boolean isUsernameValid(String username) {
        return StringUtils.hasText(username) && username.matches("^[a-zA-Z0-9_-]{3,16}$");
    }
    /**
     * Metoda sprawdzająca poprawność formatu hasła użytkownika
     * @param password hasło użytkownika
     * @return true, jeśli hasło użytkownika jest poprawne, jeśli nie false
     */

    //hasło musi zawierać co najmniej jedną literę oraz jedną cyfrę, może zawierać jedynie litery i cyfry i musi mieć co najmniej 8 znaków.
    private boolean isPasswordValid(String password) {
        return StringUtils.hasText(password) && password.matches("^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$");
    }
    /**
     * Metoda sprawdzająca poprawność pełnej nazwy użytkownika
     * @param fullUsername pełna nazwa użytkownika
     * @return true, jeśli pełna nazwa użytkownika jest poprawna, jeśli nie false
     */
    //pełna nazwa użytkownika musi mieć od 3 do 50 znaków.
    private boolean isFullUsernameValid(String fullUsername) {
        return StringUtils.hasText(fullUsername) && fullUsername.length() >= 3 && fullUsername.length() <= 50;
    }
    /**
     * Metoda sprawdzająca poprawność formatu emaila użytkownika
     * @param email mail użytkownika
     * @return true, jeśli masil użytkownika jest poprawny, jeśli nie false
     */

    //email musi spełniać standardowy format e-maila.
    private boolean isEmailValid(String email) {
        return StringUtils.hasText(email) && email.matches("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}$");
    }
}
