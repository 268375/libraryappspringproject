package org.example.libraryappbasic.service;
import org.example.libraryappbasic.entities.Book;
import org.example.libraryappbasic.entities.Loan;
import org.example.libraryappbasic.entities.User;
import org.example.libraryappbasic.error.IncompleteLoanDetailsException;
import org.example.libraryappbasic.error.InvalidDataFormatException;
import org.example.libraryappbasic.repositories.BookRepository;
import org.example.libraryappbasic.repositories.LoanRepository;
import org.example.libraryappbasic.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import java.time.LocalDate;

/**
 * Serwis obsługujący operacje na wypożyczeniach książek
 */
@Service
public class LoanService {
    private LoanRepository loanRepository;
    private BookRepository bookRepository;
    private UserRepository userRepository;


    @Value("${jwt.token.key}")
    private String key;

    /**
     * Konstruktor serwisu LoanService
     * @param loanRepository repozytorium wypożyczeń
     * @param bookRepository repozytorium książek
     * @param userRepository repozytorium użytkowników
     */
    @Autowired
    public LoanService(LoanRepository loanRepository, BookRepository bookRepository, UserRepository userRepository) {
        this.loanRepository = loanRepository;
        this.bookRepository = bookRepository;
        this.userRepository = userRepository;
    }

    /**
     * Metoda do zapisu nowego wypożyczenia książki
     * @param loan nowe wypożyczenie
     * @return zapisane wypożyczenie
     * @throws IncompleteLoanDetailsException wyjątek, gdy dodane wypożyczenie będzie mieć niekompletne dane
     * @throws InvalidDataFormatException wyjątek, gdy dane wypożyczenia mają nieprawidłowy format
     * @throws ResponseStatusException wyjątek, gdy wystąpi błąd HTTP
     */
    public Loan save(Loan loan) {
        Book book = loan.getBook();
        User user = loan.getUser();
        //jeśli nie zostaną wprowadzone wszystkie informacje na temat wypożyczenia
        if(book.getId() == null || user.getId() == null || loan.getLoanDate() == null || loan.getDeadlineDate() == null){
            throw IncompleteLoanDetailsException.create();
        }
        if (!isValidDateFormat(loan.getLoanDate())) {
            throw InvalidDataFormatException.create("loanDate");
        }
        if (!isValidDateFormat(loan.getDeadlineDate())) {
            throw InvalidDataFormatException.create("deadlineDate");
        }
        //sprawdzenie poprawności daty wypożyczenia i deadline'u
        checkDates(loan.getLoanDate(), loan.getDeadlineDate());
        //sprawdzenie czy książka o danym id, która ma zostać wypożyczona istnieje
        Book existingBook = bookRepository.findById(book.getId()).orElse(null);
        if (existingBook == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Book not found.");
        }
        //sprawdzenie, czy użytkownik o danym id istnieje w bazie danych
        User existingUser = userRepository.findById(user.getId()).orElse(null);
        if (existingUser == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found.");
        }
        //jeśli książka zostaje wypożyczona to ilość dostęnych egzemplarzy jest aktualizowana
        int availableCopies = existingBook.getAvailableCopies();
        if (availableCopies > 0) {
            existingBook.setAvailableCopies(availableCopies - 1);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "No available copies of the book.");
        }

        bookRepository.save(existingBook);
        return loanRepository.save(loan);
    }

    /**
     * Metoda zwracająca wszystkie wypożyczenia
     * @return wszystkie wypożyczenia
     */
    public Iterable<Loan> findAll() {                       //wyświetlenie wszystkich książek
        return loanRepository.findAll();
    }

    /**
     * Metoda do zwracania wypożyczenia książki
     * @param loanId identyfikator wypożyczenia
     * @param returnDate data zwrotu książki
     * @return zaktualizowane wypożyczenie o datę zwrotu
     * @throws ResponseStatusException wyjątek pojawiający się, gdy wystąpi błąd HTTP
     */
    //dopisanie daty zwrotu książki, po jej oddaniu i aktualizacja liczby dostępnych kopii książki
    public Loan returnLoan(Integer loanId, LocalDate returnDate) {
        Loan loan = loanRepository.findById(loanId).orElse(null);
        if (loan == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Loan not found.");
        }
        //sprawdzenie czy data zwrotu jest dzisiejszą datą
        if (!isToday(returnDate)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Return date must be today's date.");
        }
        //zaktualizowanie daty zwrotu po oddaniu książki
        loan.setReturnDate(returnDate);

        //zaktualizowanie liczby dostęnych kopii książki po oddaniu wypożyczenia
        Book book = loan.getBook();
        int availableCopies = book.getAvailableCopies();
        book.setAvailableCopies(availableCopies + 1);
        bookRepository.save(book);
        return loanRepository.save(loan);
    }

    /**
     * Metoda zwracająca wypożyczenia dla konkretnego użytkownika
     * @param userId identyfikator użytkownika
     * @return wypożyczenia dla konkretnego użytkownika
     */
    public Iterable<Loan> findByUserId(Integer userId) {
        return loanRepository.findByUserId(userId);
    }

    /**
     * Metoda zwracająca wypozyczenia danej książki
     * @param bookId identyfikator książki
     * @return wypożyczenia dla danej książki
     */
    public Iterable<Loan> findByBookId(Integer bookId) {
        return loanRepository.findByBookId(bookId);
    }

    /**
     * Metoda sprawdzająca, czy podana data jest późniejsza lub równa dzisiejszej
     * @param date data do sprawdzenia
     * @return tru, jeśli podana data jest późniejsza lub równa dzisiejszej, w przeciwnym razie false
     */
    private boolean isValidDateFormat(LocalDate date) {
        return !date.isBefore(LocalDate.now());
    }

    /**
     * Metoda sprawdzająca, czy podana data jest dzisiejszą datą
     * @param date data do sprawdzenia
     * @return true, jeśli data jest dzisiejszą datą, w przeciwnym razie false
     */
    private boolean isToday(LocalDate date) {
        return date.equals(LocalDate.now());
    }

    /**
     * Metoda sprawdzająca, czy podana data jest po dzisiejszej dacie
     * @param date data do sprawdzenia
     * @return true, jeśloi podana data jest po dzisiejszej dacie, w przeciwnym razie false
     */
    private boolean isAfterToday(LocalDate date) {
        return date.isAfter(LocalDate.now());
    }

    /**
     * Metoda sprawdzająca poprawność daty wypożyczenia i deadlinu
     * @param loanDate data wypożyczenia
     * @param deadlineDate data deadline'u
     * @throws ResponseStatusException wyjątek z komunikatem błędu, jeśli daty nie spełniają określonych warunków
     */
    private void checkDates(LocalDate loanDate, LocalDate deadlineDate) {
        if (!isToday(loanDate)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Loan date must be today's date.");
        }
        if (!isAfterToday(deadlineDate)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Deadline date must be after today's date.");
        }
    }



}




