package org.example.libraryappbasic.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
/**
 * Konfiguracja CORS dla aplikacji Spring MVC.
 */

@Configuration
@EnableWebMvc
public class CorsConfiguration{
    /**
     * Konfiguracja globalnych ustawień CORS dla aplikacji
     * Umożliwia żądania z dowolnego źródła oraz dowolne metody HTTP
     * @return obiekt implementujący interfejs WebMvcConfigurer
     */
    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**").allowedMethods("*").allowedOrigins("*");
            }
        };
    }
}