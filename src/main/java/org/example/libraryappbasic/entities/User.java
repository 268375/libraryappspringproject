package org.example.libraryappbasic.entities;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;

import java.util.Set;

/**
 * Reprezentuje encję użytkownika biblioteki
 */
@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @NotBlank(message = "userName is required")
    @Schema(name = "userName", example = "userName")
    private String userName;
    @NotBlank(message = "password is required")
    @Schema(name = "password", example = "password123")
    private String password;
    @NotBlank(message = "role is required")
    @Schema(name = "role", example = "ROLE_LIBRARIAN")
    @Enumerated(EnumType.STRING) // ustawienie roli użytkownika jako enum
    private EnumRoleUser role;
    @NotBlank(message = "email is required")
    @Schema(name = "email", example = "email@gmail.com")
    private String email;
    @NotBlank(message = "fullUserName is required")
    @Schema(name = "fullUserName", example = "fullUserName")
    private String fullUserName;
    @JsonIgnore //adnotacja, w celu braku wyświetlenia,podczas pokazywania getAlUsers
    @OneToMany(mappedBy = "user")
    private Set<Loan> loans;

    /**
     * Pobiera id użytkownika
     * @return id użytkownika
     */
    public Integer getId() {return id;}

    /**
     * Ustawia id użytkownika
     * @param id id użytkownika do ustawienia
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Pobiera nazwę użytkownika
     * @return nazwa użytkownika
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Ustawia nazwę użytkownika
     * @param userName nazwa użytkownika do ustawienia
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * Pobiera hasło użytkownika
     * @return hasło użytkownika
     */
    public String getPassword() {
        return password;
    }

    /**
     * Ustawia hasło użytkownika
     * @param password hasło użytkownika do ustawienia
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Pobiera rolę użytkownika
     * @return rola użytkownika
     */
    public EnumRoleUser getRole() {
        return role;
    }

    /**
     * Ustawia rolę użytkownika
     * @param role rola użytkownika do ustawienia
     */
    public void setRole(EnumRoleUser role) {
        this.role = role;
    }

    /**
     * Pobiera email użytkownika
     * @return email użytkownika
     */

    public String getEmail() {
        return email;
    }

    /**
     * Ustawia email użytkownika
     * @param email email użytkownika do ustawienia
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Pobiera pełną nazwę użytkownika
     * @return pełna nazwa użytkownika
     */
    public String getFullUserName() {
        return fullUserName;
    }

    /**
     * Ustawia pełną nazwę użytkownika
     * @param fullUserName pełna nazwa użytkownika do ustawienia
     */
    public void setFullUserName(String fullUserName) {
        this.fullUserName = fullUserName;
    }

    /**
     * Pobiera zbiór wypożyczeń dla danego użytkownika
     * @return zbiór wypożyczeń danego użytkownika
     */
    public Set<Loan> getLoans() {
        return loans;
    }

    /**
     * Ustawia zbiór wypożyczeń dla danego użytkownika
     * @param loans zbiór wypożyczeń ustawiony dla danego użytkownika
     */
    public void setLoans(Set<Loan> loans) {
        this.loans = loans;
    }

}
