package org.example.libraryappbasic.entities;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;

import java.time.LocalDate;

/**
 * Reprezentuje encję wypożyczenia w bibliotece
 */
@Entity
public class Loan {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
    @ManyToOne
    @JoinColumn(name = "book_id")
    private Book book;
    @NotBlank(message = "loanDate is required")
    @Schema(name = "loanDate", example = "2024-04-08")
    private LocalDate loanDate;
    @NotBlank(message = "deadlineDate is required")
    @Schema(name = "deadlineDate", example = "2024-04-15")
    private LocalDate deadlineDate;
    @Column(nullable = true)    //rzeczywista data zwrotu może być nullem
    private LocalDate returnDate;

    /**
     * Pobiera id wypożyczenia
     * @return id wypożyczenia
     */
    public Integer getId() {
        return id;
    }

    /**
     * Ustawia id wypożyczenia
     * @param id id do ustawienia dla wypożyczenia
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Pobiera dane użytkownika wypożyczającego książkę
     * @return dane użytkownika wypożyczającego książkę
     */
    public User getUser() {
        return user;
    }

    /**
     * Ustawia dane użytkownika wypożyczającego książkę
     * @param user dane użytkownika wypożyczającego ksiażkę
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * Pobiera dane wypożyczanej książki
     * @return dane wypożyczanej książki
     */
    public Book getBook() {return book;}

    /**
     * Ustawia dane wypożyczonej książki
     * @param book dane książki wypożyczanej
     */
    public void setBook(Book book) {
        this.book = book;
    }

    /**
     * Pobiera datę wypożyczenia
     * @return data wypożyczenia książki
     */
    public LocalDate getLoanDate() {
        return loanDate;
    }

    /**
     * Ustawia datę wypożyczenia książki
     * @param loanDate data wypożyczenia książki do ustawienia
     */
    public void setLoanDate(LocalDate loanDate) {this.loanDate = loanDate;}

    /**
     * Pobiera datę wyznaczonego terminu zwrotu książki
     * @return wyznaczony termin zwrotu książki
     */
    public LocalDate getDeadlineDate() {return deadlineDate;}

    /**
     * Ustawia datę wyznaczonego terminu zwrotu książki
     * @param deadlineDate wyznaczony termin zwrotu książki
     */
    public void setDeadlineDate(LocalDate deadlineDate) {this.deadlineDate = deadlineDate;}

    /**
     * Pobiera datę faktycznego zwrotu książki
     * @return data faktycznego zwrotu książki
     */
    public LocalDate getReturnDate() {return returnDate;}

    /**
     * Ustawia datę faktycznego zwrotu książki
     * @param returnDate data faktycznego zwrotu książi do ustawienia
     */
    public void setReturnDate(LocalDate returnDate) {this.returnDate = returnDate;}

}
