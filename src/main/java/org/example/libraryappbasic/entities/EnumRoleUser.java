package org.example.libraryappbasic.entities;

/**
 * Reprezentuje role użytkowników w bibliotece
 */
public enum EnumRoleUser {
    /**
     * Rola czytelnika
     */
    ROLE_READER,
    /**
     * Rola bibliotekarza
     */
    ROLE_LIBRARIAN
}
