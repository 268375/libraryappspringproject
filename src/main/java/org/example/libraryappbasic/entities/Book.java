package org.example.libraryappbasic.entities;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;

import java.util.Set;

/**
 * Klasa reprezentująca encję książki w aplikacji biblioteki
 */

@Entity
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @NotBlank(message = "isbn is required")
    @Schema(name = "isbn", example = "1234567890123")
    private String isbn;
    @NotBlank(message = "title is required")
    @Schema(name = "title", example = "title")
    private String title;
    @NotBlank(message = "author is required")
    @Schema(name = "author", example = "author")
    private String author;
    @NotBlank(message = "publisher is required")
    @Schema(name = "publisher", example = "publisher")
    private String publisher;
    @NotBlank(message = "publishYear is required")
    @Schema(name = "publishYear", example = "2022")
    private Integer publishYear;
    @NotBlank(message = "availableCopies are required")
    @Schema(name = "availableCopies", example = "10")
    private Integer availableCopies;
    @JsonIgnore //adnotacja, w celu braku wyświetlenia,podczas pokazywania getAllBooks
    @OneToMany(mappedBy = "book")
    private Set<Loan> loans;

    /**
     * Pobiera id książki
     * @return id książki
     */
    public Integer getId() {
        return id;
    }

    /**
     * Ustawia id książki
     * @param id id do ustawienia dla książki
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Pobiera ISBN książki
     * @return ISBN książki
     */
    public String getIsbn() {
        return isbn;
    }

    /**
     * Ustawia ISBN książki
     * @param isbn ISBN do ustawienia dla książki
     */
    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    /**
     * Pobiera tytuł książki
     * @return tytuł książki
     */
    public String getTitle() {
        return title;
    }

    /**
     * Ustawia tytuł książki
     * @param title tytuł książki
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Pobiera autora książki
     * @return autor książki
     */
    public String getAuthor() {
        return author;
    }

    /**
     * Ustawia autora książki
     * @param author autor książki
     */
    public void setAuthor(String author) {
        this.author = author;
    }

    /**
     * Pobiera wydawcę książki
     * @return wydawca książki
     */
    public String getPublisher() {
        return publisher;
    }

    /**
     * Ustawia wydawcę książki
     * @param publisher wydawca książki do ustawienia
     */
    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    /**
     * Pobiera rok wydania książki
     * @return rok wydania książki
     */
    public Integer getPublishYear() {
        return publishYear;
    }

    /**
     * Ustawia rok wydania książki
     * @param publishYear rok wydania książki do ustawienia
     */
    public void setPublishYear(Integer publishYear) {
        this.publishYear = publishYear;
    }

    /**
     * Pobiera liczbę dostępnych egzemplarzy książki
     * @return liczba dostępnych egzemplarzy książki
     */
    public Integer getAvailableCopies() {
        return availableCopies;
    }

    /**
     * Ustawia liczbę dostępnych egzemplarzy książki
     * @param availableCopies liczba dostęnych egzemplarzy do ustawienia dla książki
     */
    public void setAvailableCopies(Integer availableCopies) {
        this.availableCopies = availableCopies;
    }

    /**
     * Pobiera zbiór wypożyczeń związanych z książką
     * @return zbiór wypożyczeń powiązanych z książką
     */
    public Set<Loan> getLoans() {
        return loans;
    }

    /**
     * Ustawia zbiór wypożyczeń związanych z książką
     * @param loans zbiór wypożyczeń do ustawienia dla książki
     */
    public void setLoans(Set<Loan> loans) {
        this.loans = loans;
    }

}