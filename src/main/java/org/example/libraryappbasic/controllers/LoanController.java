package org.example.libraryappbasic.controllers;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.example.libraryappbasic.entities.Loan;
import org.example.libraryappbasic.error.IncompleteLoanDetailsException;
import org.example.libraryappbasic.error.InvalidDataFormatException;
import org.example.libraryappbasic.service.LoanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import java.time.LocalDate;
/**
 * Klasa LoanController obsługuje żądania HTTP związane z wykonywaniem operacji na wypożyczeniach.
 * Zawiera endpointy do dodawania, wyszukiwania, zwracania i wyświetlania wypożyczeń.
 */
@RestController
@RequestMapping("/loan")
public class LoanController {
    private final LoanService loanService;

    /**
     * Utworzenie nowego kontrolera LoanController z zadanym LoanService
     * @param loanService - usługa odpowiedzialna za operacje na wypożyczeniach
     */
    @Autowired
    public LoanController (LoanService loanService){
        this.loanService = loanService;
    }
    /**
     * Endpoint do dodawania wypożyczenia przez bibliotekarza (dostępny tylko dla bibliotekarza)
     * Aktualizuje liczbę dostępnych egzemplarzy książki
     * @param loan wypożyczenie do dodania
     * @return dodane wypożyczenie
     * @throws ResponseStatusException jeśli dane wypożyczenia są niekompletne lub niepoprawne
     */
    @PreAuthorize("hasRole('ROLE_LIBRARIAN')")
    @PostMapping("/add")
    @ResponseStatus(code = HttpStatus.CREATED)
    @ApiResponses(value ={
            @ApiResponse(responseCode = "201", description = "New loan created"),
            @ApiResponse(responseCode = "400", description = "Incomplete information about loan", content = @Content),
            @ApiResponse(responseCode = "400", description = "Invalid information about loan", content = @Content)
    })
    public @ResponseBody Loan addLoan(@RequestBody Loan loan) {
        try {
            return loanService.save(loan);

        }catch (IncompleteLoanDetailsException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        } catch (InvalidDataFormatException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
    /**
     * Endpoint do pobierania wszystkich wypożyczeń dostępny tylko dla bibliotekarza
     * @return lista wszystkich wypożyczeń
     */
    @PreAuthorize("hasRole('ROLE_LIBRARIAN')")
    @GetMapping("/getAll")
    @ApiResponses(value ={
            @ApiResponse(responseCode = "200", description = "Get all loans succeeded")
    })
    public @ResponseBody Iterable<Loan> getAllLoans(){
        return loanService.findAll();
    }
    /**
     * Endpoint do zwracania wypożyczenia przez bibliotekarza dostęny tylko dla bibliotekarza
     * Umożliwia ustawienie daty zwrotu książki i aktualizację liczby dostęnych egzemplarzy książki
     * @param loanId ID wypożyczenia do zwrócenia
     * @param returnDate data zwrotu książki
     * @return zwrócone wypożyczenie
     */
    @PreAuthorize("hasRole('ROLE_LIBRARIAN')")
    @PutMapping("/return/{loanId}")
    @ApiResponses(value ={
            @ApiResponse(responseCode = "200", description = "Return loan succeeded"),
            @ApiResponse(responseCode = "400", description ="Return loan failed", content = @Content)
    })
    public @ResponseBody Loan returnLoan(
            @PathVariable Integer loanId,
            @RequestParam("returnDate") LocalDate returnDate) {
        try {
            return loanService.returnLoan(loanId, returnDate);
        }catch(IllegalArgumentException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    /**
     * Endpoint do pobierania wypożyczeń aktualnie zalogowanego użytkownika
     * Dostępny dla czytelnika i bibliotekarza
     * @return lista wypożyczeń aktualnego użytkownika
     */
    @PreAuthorize("hasAnyRole('ROLE_LIBRARIAN', 'ROLE_READER')")
    @GetMapping("/myLoans")
    @ApiResponses(value ={
            @ApiResponse(responseCode = "200", description = "Loading your loans succeeded")
    })
    public @ResponseBody Iterable<Loan> getLoansForCurrentUser() {
        //pobranie ID użytkownika z kontekstu uwierzytelnienia
        Integer currentUserId = getCurrentUserId();
        return loanService.findByUserId(currentUserId);
    }

    /**
     * Endpoint do pobierania wypożyczeń dla danego użytkownika przez bibliotekarza
     * @param userId ID użytkownika
     * @return lista wypożyczeń dla danego użytkownika
     * @throws ResponseStatusException jeśli nie znaleziono wypożyczeń dla danego użytkownika
     */
    @PreAuthorize("hasRole('ROLE_LIBRARIAN')")
    @GetMapping("/userLoans/{userId}")
    @ApiResponses(value ={
            @ApiResponse(responseCode = "200", description = "Load loans for user succeeded"),
            @ApiResponse(responseCode = "404", description = "Loan for user not found", content = @Content)
    })
    public @ResponseBody Iterable<Loan> getLoansByUserId(@PathVariable Integer userId) {
        Iterable<Loan> loans = loanService.findByUserId(userId);
        if (!loans.iterator().hasNext()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No loans found for user with ID: " + userId);
        }
        return loans;
    }

    /**
     * Endpoint do pobierania wypożyczeń dla danej książki przez bibliotekarza
     * @param bookId ID książki
     * @return lista wypożyczeń dla danej książki
     * @throws ResponseStatusException jeśli nie znaleziono wypożyczeń dla danej książki
     */

    @PreAuthorize("hasRole('ROLE_LIBRARIAN')")
    @GetMapping("/bookLoans/{bookId}")
    @ApiResponses(value ={
            @ApiResponse(responseCode = "200", description = "Load loans for book succeeded"),
            @ApiResponse(responseCode = "404", description = "Loan for book not found", content = @Content)
    })
    public @ResponseBody Iterable<Loan> getLoansByBookId(@PathVariable Integer bookId) {
        Iterable<Loan> loans = loanService.findByBookId(bookId);
        if (!loans.iterator().hasNext()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No loans found for book with ID: " + bookId);
        }
        return loans;
    }


    /**
     * Pobiera ID użytkownika z kontekstu uwierzytelnienia
     * @return ID użytkownika
     */
    private Integer getCurrentUserId() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return Integer.parseInt(authentication.getName()); // ID użytkownika jest ustawiane jako nazwa użytkownika w Authentication
    }



}





