package org.example.libraryappbasic.controllers;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.security.SecurityRequirements;
import org.example.libraryappbasic.LoginForm;
import org.example.libraryappbasic.error.WrongLoginDetailsException;
import org.example.libraryappbasic.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
/**
 * Klasa LoginController obsługuje żądania HTTP związane z procesem logowania się użytkownika do systemu bibliotecznego
 */
@RestController
public class LoginController {
    private final LoginService loginService;
    /**
     * Utworzenie nowego kontrolera LoginController z zadanym LoginService
     * @param loginService - usługa odpowiedzialna za proces logowania się użytkowników
     */
    @Autowired
    public LoginController(LoginService loginService) {
        this.loginService = loginService;
    }
    /**
     * Endpoint do logowania się użytkownika.
     * @param loginForm obiekt LoginForm zawierający dane logowania użytkownika
     * @return ResponseEntity zawierający token uwierzytelniający, jeśli logowanie zakończyło się sukcesem
     * @throws ResponseStatusException jeśli podane dane logowania (login i/lub hasło) są nieprawidłowe
     */
    @PostMapping("/login")
    @PreAuthorize("permitAll()")
    @SecurityRequirements
    @ApiResponses(value ={
            @ApiResponse(responseCode = "200", description = "Login succeeded"),
            @ApiResponse(responseCode = "401", description = "Login failed", content = @Content)
    })
    public ResponseEntity<String> login(@RequestBody LoginForm loginForm) {
        String token;
        try {
            token = loginService.userLogin(loginForm);
        } catch (WrongLoginDetailsException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Wrong login and/or password", e);
        }
        return new ResponseEntity<>(token, HttpStatus.OK);
    }
}