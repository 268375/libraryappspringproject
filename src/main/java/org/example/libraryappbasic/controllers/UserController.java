package org.example.libraryappbasic.controllers;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.example.libraryappbasic.entities.User;
import org.example.libraryappbasic.error.IncompleteUserDetailsException;
import org.example.libraryappbasic.error.InvalidDataFormatException;
import org.example.libraryappbasic.error.UserAlreadyExistsException;
import org.example.libraryappbasic.error.UserDoesntExistException;
import org.example.libraryappbasic.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
/**
 * Klasa UserController obsługuje żądania HTTP związane z wykonywaniem operacji na użytkownikach.
 * Zawiera endpointy do dodawania, usuwania, wyświetlania, aktualizowania danych i wyszukiwania użytkowników
 */
@RestController
@RequestMapping("/user")
public class UserController {
    private final UserService userService;
    /**
     * Utworzenie nowego kontrolera userController z zadanym UserService
     * @param userService - usługa odpowiedzialna za operacje na użytkownikach
     */
    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }
    /**
     * Endpoint do dodawania użytkownika dostępny tylko dla bibliotekarza
     * @param user użytkownik do dodania
     * @return dodany użytkownik.
     * @throws ResponseStatusException jeśli próba dodania użytkownika zakończy się niepowodzeniem (niekompletne lub niepoprawne dane użytkownika, albo użytkownik już istnieje)
     */
    @PreAuthorize("hasRole('ROLE_LIBRARIAN')")
    @PostMapping("/add")
    @ResponseStatus(code = HttpStatus.CREATED)
    @ApiResponses(value ={
            @ApiResponse(responseCode = "201", description = "New user created"),
            @ApiResponse(responseCode = "409", description = "User already exists", content = @Content),
            @ApiResponse(responseCode = "400", description = "Incomplete information about user", content = @Content),
            @ApiResponse(responseCode = "400", description = "Invalid information about user", content = @Content)
    })
    public @ResponseBody User addUser(@RequestBody User user) {
        try{
            return userService.save(user);
        }catch (UserAlreadyExistsException e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());     //jeśli chcemy dodać użytkownika, który już istnieje
        } catch (IncompleteUserDetailsException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());     //jeśli chcemy dodać użytkownika, ale nie podamy wszystkich informacji
        } catch (InvalidDataFormatException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }

    }
    /**
     * Endpoint do wyszukiwania użytkownika po nazwie użytkownika dostępny tylko dla bibliotekarza
     * @param userName nazwa użytkownika do wyszukania
     * @return dane użytkownika o podanej nazwie użytkownika
     * @throws ResponseStatusException jeśli użytkownik o podanej nazwie nie istnieje
     */
    @PreAuthorize("hasRole('ROLE_LIBRARIAN')")
    @GetMapping("/findByUserName/{userName}")
    @ApiResponses(value ={
            @ApiResponse(responseCode = "200", description = "Find user was succeed"),
            @ApiResponse(responseCode = "404", description = "User not found", content = @Content)
    })
    public @ResponseBody User getUserByUserName(@PathVariable String userName){
        User user = userService.findByUserName(userName);
        if(user == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found");
        }
        return user;
        }

    /**
     * Endpoint do usuwania użytkownika dostępny tylko dla bibliotekarza
     * @param userId ID użytkownika do usunięcia
     * @throws ResponseStatusException jeśli użytkownik o podanym ID nie istnieje
     */
    @PreAuthorize("hasRole('ROLE_LIBRARIAN')")
    @DeleteMapping("/delete/{userId}")
    @ApiResponses(value ={
            @ApiResponse(responseCode = "200", description = "Delete user was succeed"),
            @ApiResponse(responseCode = "404", description = "User not found", content = @Content)
    })

    public void deleteUser(@PathVariable Integer userId) {
        try {
            userService.deleteById(userId);
        } catch (UserDoesntExistException e) {                                      //jeśli użytkownik, którego chcemy usunąć nie istnieje
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
    /**
     * Endpoint do wyświetlania wszystkich użytkowników dostępny tylko dla bibliotekarza
     * @return lista wszystkich użytkowników biblioteki
     */
    @PreAuthorize("hasRole('ROLE_LIBRARIAN')")
    @GetMapping("/getAll")
    @ApiResponses(value ={
            @ApiResponse(responseCode = "200", description = "Get all users succeeded")
    })
    public @ResponseBody Iterable<User> getAllUsers() {
        return userService.findAll();
    }



    /**
     * Endpoint do aktualizowania (modyfikowania) własnych danych użytkownika dostępny dla czytelnika i bibliotekarza
     * @param updatedUserData dane do zaktualizowania
     * @return zaktualizowane dane użytkownika.
     * @throws ResponseStatusException jeśli uaktualizowane dane mają nieprawidłowy format lub nowa nazwa użytkownika już istnieje w bazie danych
     */
    @PreAuthorize("hasRole('ROLE_READER') or hasRole('ROLE_LIBRARIAN')")
    @PutMapping("/update")
    @ApiResponses(value ={
            @ApiResponse(responseCode = "200", description = "Upadate user information was succeed"),
            @ApiResponse(responseCode = "400", description = "Invalid data format", content = @Content),
            @ApiResponse(responseCode = "409", description = "User with this userename already exists", content = @Content)
    })

    public @ResponseBody User updateUser(@RequestBody User updatedUserData) {
        //pobranie ID użytkownika z kontekstu uwierzytelnienia
        try {
            Integer currentUserId = getCurrentUserId();
            return userService.updateUser(currentUserId, updatedUserData);
        } catch (InvalidDataFormatException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        } catch (UserAlreadyExistsException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }
    /**
     * Endpoint do wyświetlania danych aktualnie zalogowanego użytkownika dostępny dla czytelnika i bibliotekarza
     * @return dane aktualnie zalogowanego użytkownika.
     * @throws ResponseStatusException jeśli użytkownik nie istnieje
     */
    @PreAuthorize("hasRole('ROLE_READER') or hasRole('ROLE_LIBRARIAN')")
    @GetMapping("/InfoAboutMe")
    @ApiResponses(value ={
            @ApiResponse(responseCode = "200", description = "Get information about user was succeed"),
            @ApiResponse(responseCode = "404", description = "User not found", content = @Content)
    })
    public User getCurrentUser() {
        try {
            Integer currentUserId = getCurrentUserId();
            return userService.findById(currentUserId)
                    .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Current user does not exist"));
        } catch (UserDoesntExistException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
    /**
     * Pobiera ID użytkownika z kontekstu uwierzytelnienia
     * @return ID użytkownika
     */
    private Integer getCurrentUserId() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return Integer.parseInt(authentication.getName());
    }

    /**
     * Endpoint do aktualizowania danych innego użytkownika dostępny tylko dla bibliotekarza
     * @param userId ID użytkownika do zaktualizowania danych
     * @param updatedUserData zaktualizowane dane użytkownika
     * @return zaktualizowane dane użytkownika.
     * @throws ResponseStatusException jeśli użytkownik o podanym ID nie istnieje lub zaktualizowane dane mają nieprawidłowy format lub nowa nazwa użytkownika już istnieje w bazie danych
     */

    @PreAuthorize("hasRole('ROLE_LIBRARIAN')")
    @PutMapping("/updateOther/{userId}")
    @ApiResponses(value ={
            @ApiResponse(responseCode = "200", description = "Update succeeded"),
            @ApiResponse(responseCode = "404", description = "User to update not found", content = @Content),
            @ApiResponse(responseCode = "400", description = "Invalid data format", content = @Content),
            @ApiResponse(responseCode = "409", description = "User with this userename already exists", content = @Content)
    })
    public @ResponseBody User updateOtherUser(@PathVariable Integer userId, @RequestBody User updatedUserData) {
        try {
            return userService.updateOtherUser(userId, updatedUserData);
        } catch (UserDoesntExistException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (InvalidDataFormatException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }catch(UserAlreadyExistsException e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }

    }


}



