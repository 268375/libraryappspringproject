package org.example.libraryappbasic.controllers;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.example.libraryappbasic.entities.Book;
import org.example.libraryappbasic.entities.User;
import org.example.libraryappbasic.error.*;
import org.example.libraryappbasic.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

/**
 * Klasa BookController obsługuje żądania HTTP związane z wykonywaniem operacji na książach.
 * Zawiera endpointy do dodawania, usuwania i wyszukiwania książek na podstawie różnych kryteriów.
 */
@RestController
@RequestMapping("/book")
public class BookController {
    private final BookService bookService;

    /**
     * Utworzenie nowego kontrolera BookController z zadanym BookService
     * @param bookService - usługa odpowiedzialna za operacje na książkach
     */
    @Autowired
    public BookController (BookService bookService){
        this.bookService = bookService;
    }

    /**
     * Endpoint do dodania ksiażki dostęny tylko dla bibliotekarza
     * @param book - książka do dodania
     * @return - dodana do bazy książka
     * @throws ResponseStatusException - w przypadku, gdy książka o podanym ISBN juz istnieje w bazie danych lub jeśli nie zostaną wprowadzone wszystkie informacje o książce lub jeśli dane dotyczące książki są niepoprawne
     */
    @PreAuthorize("hasRole('ROLE_LIBRARIAN')")
    @PostMapping("/add")
    @ResponseStatus(code = HttpStatus.CREATED)
    @ApiResponses(value ={
            @ApiResponse(responseCode = "201", description = "New book created"),
            @ApiResponse(responseCode = "409", description = "Book already exists", content = @Content),
            @ApiResponse(responseCode = "400", description = "Incomplete information about book", content = @Content),
            @ApiResponse(responseCode = "400", description = "Invalid information about book", content = @Content)
    })
    public @ResponseBody Book addBook(@RequestBody Book book){
        try{
            return bookService.save(book);
        }catch (BookAlreadyExistsException e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());     //jeśli chcemy dodać książkę, która już istnieje
        } catch (IncompleteBookDetailsException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());     //jeśli chcemy dodać książkę, ale nie podamy wszystkich informacji
        } catch (InvalidDataFormatException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }

    }

    /**
     * Endpoint do usunięcia ksiażki dostęny tylko dla bibliotekarza
     * @param bookId - id książki, która ma zostać usunięta
     * @throws ResponseStatusException - jeśli książka o danym id nie istnieje w bazie danych
     */
    @PreAuthorize("hasRole('ROLE_LIBRARIAN')")
    @DeleteMapping("/delete/{bookId}")
    @ApiResponses(value ={
            @ApiResponse(responseCode = "200", description = "Delete book was succeed"),
            @ApiResponse(responseCode = "404", description = "Book not found", content = @Content)
    })
    public void deleteBook(@PathVariable Integer bookId) {
        try {
            bookService.deleteById(bookId);
        } catch (BookDoesntExistException e) {                                      //jeśli użytkownik, którego chcemy usunąć nie istnieje
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    /**
     * Endpoint do pobierania wszystkich ksiażek dostęny zarówno dla bibliotekarza, jak i dla czytelnika
     * @return dane o wszystkich ksiażach znajdujących się w bibliotece
     */
    @PreAuthorize("hasAnyRole('ROLE_LIBRARIAN', 'ROLE_READER')")  //endpoint do wyświetlenia wszystkich książek - ma do niego dostęp zarówno bibliotekarz i czytelnik
    @GetMapping("/getAll")
    @ApiResponses(value ={
            @ApiResponse(responseCode = "200", description = "Get all books succeeded")
    })
    public @ResponseBody Iterable<Book> getAllBooks() {
        return bookService.findAll();
    }

    /**
     * Endpoint do wyszukiwania książek na podstawie tytułu dostępny zarówno dla bibliotekarza, jak i dla czytelnika
     * @param title - tytuł książki, która ma zostać wyszukana
     * @return lista książek spełniających kryterium wyszukiwania
     * @throws ResponseStatusException jeśli nie znaleziono książek o podanym tytule
     */
    @PreAuthorize("hasAnyRole('ROLE_LIBRARIAN', 'ROLE_READER')")
    @GetMapping("/searchByTitle/{title}")
    @ApiResponses(value ={
            @ApiResponse(responseCode = "200", description = "Book found succeeded"),
            @ApiResponse(responseCode = "404", description = "Book not found", content = @Content)
    })
    public @ResponseBody Iterable<Book> searchByTitle(@PathVariable String title) {
        try {
            return bookService.findByTitle(title);
        } catch (TitleNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
    /**
     * Endpoint do wyszukiwania książek na podstawie ISBN dostępny zarówno dla bibliotekarza, jak i dla czytelnika
     * @param isbn ISBN książki, która ma zostać wyszukana
     * @return książka spełniająca kryterium wyszukiwania
     * @throws ResponseStatusException jeśli nie znaleziono książki o podanym ISBN
     */
    @PreAuthorize("hasAnyRole('ROLE_LIBRARIAN', 'ROLE_READER')")
    @GetMapping("/searchByIsbn/{isbn}")
    @ApiResponses(value ={
            @ApiResponse(responseCode = "200", description = "Book found succeeded"),
            @ApiResponse(responseCode = "404", description = "Book not found", content = @Content)
    })
    public @ResponseBody Book searchByIsbn(@PathVariable String isbn) {

        try {
            return bookService.findByIsbn(isbn);
        } catch (IsbnNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
    /**
     * Endpoint do wyszukiwania książek na podstawie roku wydania dostępny zarówno dla bibliotekarza, jak i dla czytelnika
     * @param year rok wydania książek, które mają zostać wyszukane
     * @return lista książek wydanych w podanym roku
     * @throws ResponseStatusException jeśli nie znaleziono książek wydanych w podanym roku
     */
    @PreAuthorize("hasAnyRole('ROLE_LIBRARIAN', 'ROLE_READER')")
    @GetMapping("/searchByPublishYear/{year}")
    @ApiResponses(value ={
            @ApiResponse(responseCode = "200", description = "Book found succeeded"),
            @ApiResponse(responseCode = "404", description = "Book not found", content = @Content)
    })
    public @ResponseBody Iterable<Book> searchByPublishYear(@PathVariable Integer year) {
        try {
            return bookService.findByPublishYear(year);
        } catch (PublishYearNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
    /**
     * Endpoint do wyszukiwania książek na podstawie wydawnictwa dostępny zarówno dla bibliotekarza, jak i dla czytelnika
     * @param publisher wydawnictwo książek do wyszukania
     * @return lista książek wydanych przez podane wydawnictwo
     * @throws ResponseStatusException jeśli nie znaleziono książek wydanych przez podane wydawnictwo
     */
    @PreAuthorize("hasAnyRole('ROLE_LIBRARIAN', 'ROLE_READER')")
    @GetMapping("/searchByPublisher/{publisher}")
    @ApiResponses(value ={
            @ApiResponse(responseCode = "200", description = "Book found succeeded"),
            @ApiResponse(responseCode = "404", description = "Book not found", content = @Content)
    })
    public @ResponseBody Iterable<Book> searchByPublisher(@PathVariable String publisher) {
        try {
            return bookService.findByPublisher(publisher);
        } catch (PublisherNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    /**
     * Endpoint do wyszukiwania książek na podstawie autora dostępny zarówno dla bibliotekarza, jak i dla czytelnika.
     * @param author autor książek do wyszukania
     * @return lista książek napisanych przez podanego autora
     * @throws ResponseStatusException jeśli nie znaleziono książek napisanych przez podanego autora
     */
    @PreAuthorize("hasAnyRole('ROLE_LIBRARIAN', 'ROLE_READER')")
    @GetMapping("/searchByAuthor/{author}")
    @ApiResponses(value ={
            @ApiResponse(responseCode = "200", description = "Book found succeeded"),
            @ApiResponse(responseCode = "404", description = "Book not found", content = @Content)
    })
    public @ResponseBody Iterable<Book> searchByAuthor(@PathVariable String author) {
        try{
            return bookService.findByAuthor(author);
        } catch (AuthorNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
    /**
     * Endpoint do aktualizowania danych książki dostępny tylko dla bibliotekarza
     * @param bookId ID książki do zaktualizowania danych
     * @param updatedBookData zaktualizowane dane książki
     * @return zaktualizowane dane książki
     * @throws ResponseStatusException jeśli książka o podanym ID nie istnieje lub zaktualizowane dane mają nieprawidłowy format lub nowe ISBN już istnieje w bazie danych
     */

    @PreAuthorize("hasRole('ROLE_LIBRARIAN')")
    @PutMapping("/updateBookDetails/{bookId}")
    @ApiResponses(value ={
            @ApiResponse(responseCode = "200", description = "Update succeeded"),
            @ApiResponse(responseCode = "404", description = "Book to update not found", content = @Content),
            @ApiResponse(responseCode = "400", description = "Invalid data format", content = @Content),
            @ApiResponse(responseCode = "409", description = "Book with this ISBN already exists", content = @Content)
    })
    public @ResponseBody Book updateBook (@PathVariable Integer bookId, @RequestBody Book updatedBookData) {
        try {
            return bookService.updateBook(bookId, updatedBookData);
        } catch (BookDoesntExistException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (InvalidDataFormatException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }catch(BookAlreadyExistsException e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }

    }

}



