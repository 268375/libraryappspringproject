package org.example.libraryappbasic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Główna klasa aplikacji biblioteki (umożliwia uruchomienie aplikacji Spring Book=t)
 */

@SpringBootApplication
public class LibraryAppBasicApplication {
    /**
     * Metoda główna uruchamiająca aplikację
     * @param args argumenty przekazane podczas uruchamiania aplikacji
     */

    public static void main(String[] args) {
        SpringApplication.run(LibraryAppBasicApplication.class, args);
    }

}
